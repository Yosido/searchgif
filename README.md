# SearchGif

One of many code examples, perhaps there is unit tests. Allows to search gifs using giphy API, display it in UICollectionVIew with fashoin-loooked layout, infinite scroll, and share it using other application

## Compability
Project is compatible with Xcode 9.3 or higher

#### Building

Clone from git repository [link][repositoryLink], open project file, press `cmd+R`

## How it looks like

![out.gif](out.gif)

   [repositoryLink]: <https://gitlab.com/Yosido/searchgif>
