//
//  SGDefaultGriddedContentCollectionViewDelegate.swift
//  SearchGifTDD
//
//  Created by Yosido on 13.03.2021.
//

import UIKit.UICollectionViewCell

class SGDefaultGriddedContentCollectionViewDelegate: SGDefaultCollectionViewDelegate {
    private var itemsPerRow: CGFloat = 2
    private let minimumItemSpacing: CGFloat = 16
    
    // MARK: - SGDefaultGriddedContentCollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if (UIDevice.current.userInterfaceIdiom == .phone) {
            itemsPerRow = 2
        } else if (UIDevice.current.userInterfaceIdiom == .pad) {
            itemsPerRow = 3
        }
        
        let paddingSpace = SGCollectionViewDelegateConstants.sectionInsets.left + SGCollectionViewDelegateConstants.sectionInsets.right + minimumItemSpacing * (itemsPerRow - 1)
        let availableWidth = collectionView.bounds.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        return CGSize(width: widthPerItem, height: widthPerItem * SGViewConstants.kSGPhi)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return SGCollectionViewDelegateConstants.sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return SGCollectionViewDelegateConstants.minimumLineSpacingForSectionForGridLayout
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return minimumItemSpacing
    }
}
