//
//  SGGridLayout.swift
//  SearchGifTDD
//
//  Created by Anton Sorokin on 18.03.2021.
//  Copyright © 2021 Anton Sorokin. All rights reserved.
//

import UIKit

protocol SGGridLayoutDelegate: AnyObject {
    func collectionView(
        _ collectionView: UICollectionView,
        heightForGifAtIndexPath indexPath: IndexPath) -> CGFloat
}

/// Class describes waterfall layout
class SGGridLayout: UICollectionViewLayout {
    weak var delegate: SGGridLayoutDelegate?
    private let cellPadding: CGFloat = 6
    private let defaultCellHeight: CGFloat = 150
    private var cache: [UICollectionViewLayoutAttributes] = []
    private var contentHeight: CGFloat = 0
    private var contentWidth: CGFloat {
        guard let collectionView = collectionView else {
            return 0
        }
        let insets = collectionView.contentInset
        return collectionView.bounds.width - (insets.left + insets.right)
    }
    
    override var collectionViewContentSize: CGSize {
        return CGSize(width: contentWidth, height: contentHeight)
    }
    
    override func prepare() {
        guard let collectionView = collectionView else {
            return
        }
        
        let columnCount:Int = numberOfColumns()
        let columnWidth = (contentWidth / CGFloat(columnCount)).rounded()
        var xOffset: [CGFloat] = []
        for column in 0..<columnCount {
            xOffset.append(CGFloat(column) * columnWidth)
        }
        var column = 0
        var yOffset: [CGFloat] = .init(repeating: 0, count: columnCount)
        
        for item in 0..<collectionView.numberOfItems(inSection: 0) {
            let indexPath = IndexPath(item: item, section: 0)
            
            let gifHeight = delegate?.collectionView(
                collectionView,
                heightForGifAtIndexPath: indexPath) ?? defaultCellHeight
            let height = cellPadding * 2 + gifHeight
            let frame = CGRect(x: xOffset[column],
                               y: yOffset[column],
                               width: columnWidth,
                               height: height)
            let insetFrame = frame.insetBy(dx: cellPadding, dy: cellPadding)
            
            let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            attributes.frame = insetFrame
            cache.append(attributes)
            
            contentHeight = max(contentHeight, frame.maxY).rounded()
            yOffset[column] = yOffset[column] + height
            
            column = column < (columnCount - 1) ? (column + 1) : 0
        }
    }
    
    override func layoutAttributesForElements(in rect: CGRect)
    -> [UICollectionViewLayoutAttributes]? {
        var visibleLayoutAttributes: [UICollectionViewLayoutAttributes] = []
        
        for attributes in cache {
            if attributes.frame.intersects(rect) {
                visibleLayoutAttributes.append(attributes)
            }
        }
        return visibleLayoutAttributes
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath)
    -> UICollectionViewLayoutAttributes? {
        return cache[indexPath.item]
    }
    
    func numberOfColumns() -> Int {
        if (UIDevice.current.userInterfaceIdiom == .phone) {
            return 2
        } else if (UIDevice.current.userInterfaceIdiom == .pad) {
            return 3
        } else {
            return 1
        }
    }
    
    func clearCache() {
        cache = []
    }
    
    func resetContentHeight() {
        contentHeight = 0
    }
    
    func updateLayout() {
        clearCache()
        resetContentHeight()
    }
}
