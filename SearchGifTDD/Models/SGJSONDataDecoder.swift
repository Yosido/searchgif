//
//  SGJSONDataDecoder.swift
//  SearchGifTDD
//
//  Created by Anton Sorokin on 23.03.2021.
//  Copyright © 2021 Anton Sorokin. All rights reserved.
//

import Foundation

/// Class decode Data into array of SGGIF objects
class SGJSONDataDecoder {
    private var data: Data
    /// Initializer
    /// - Parameter data: data for decoding
    init(data: Data) {
        self.data = data
    }
    
    /// Function thar decode passed data in contructor to array of SGGIF objects is success case, or return error in completion block
    /// - Parameter completion: A block called when operation has been completed.
    /// - Returns: Result<[SGGIF], Error> -- if success -- array of SGGIF objects, if failure returns error
    func decodeJSONDataToResponseObject(completion: @escaping (Result<[SGGIF], Error>) -> ()) {
        do {
            let responseObject = try JSONDecoder().decode(SGResponse.self, from: data)
            if responseObject.meta.status != 200 {
                
                completion(.failure(NetworkRequestError.api(responseObject.meta.status, SGApiResultCode.code(for: responseObject.meta.msg), responseObject.meta.msg)))
                return
            }
            
            let gifs = responseObject.gifs
            completion(.success(gifs))
        } catch  {
            completion(.failure(error))
        }
    }
}
