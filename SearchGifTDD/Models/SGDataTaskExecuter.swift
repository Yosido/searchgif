//
//  SGDataTaskExecuter.swift
//  SearchGifTDD
//
//  Created by Anton Sorokin on 23.03.2021.
//  Copyright © 2021 Anton Sorokin. All rights reserved.
//

import Foundation

protocol SGDataTaskExecuterProtocol {
    func fetchGifsRequest(completion: @escaping (Result<Data, Error>) -> ())
}

// this class used to execute URLRequest, that returns responcse contains gif's data
class SGDataTaskExecuter: SGDataTaskExecuterProtocol {
    /// request to load
    private var request:URLRequest
    /// An object that coordinates network data-transfer tasks.
    private let urlSession: URLSession
    
    init(request: URLRequest, session: URLSession = URLSession.shared) {
        self.request = request
        self.urlSession = session
    }
    /// Function thar load request using  URLSession
    /// - Parameter completion: A block called when load request is complete
    /// - Returns: Result<Data, Error> if success -- response data from giphy endpoint, if failure returns error object that indicates why the request failed, or nil if the request was successful
    func fetchGifsRequest(completion: @escaping (Result<Data, Error>) -> ()) {
        let task = urlSession.dataTask(with: request) { data, response, error in
            guard let responseData = data, error == nil else {
                completion(.failure(error ?? NetworkRequestError.unknown(data, response)))
                return
            }
            
            completion(.success(responseData))
        }
        task.resume()
    }
}
