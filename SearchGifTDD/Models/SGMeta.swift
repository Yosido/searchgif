//
//  SGMeta.swift
//  SearchGifTDD
//
//  Created by Anton Sorokin on 11.03.2021.
//  Copyright © 2021 Anton Sorokin. All rights reserved.
//

import Foundation

/// The Meta Object contains basic information regarding the response and its status.
struct SGMeta: Decodable {
    /// HTTP Response Message
    var msg: String
    /// HTTP Response Code
    var status: Int
}
