//
//  SGNetworkModel.swift
//  SearchGifTDD
//
//  Created by Anton Sorokin on 17.03.2021.
//  Copyright © 2021 Anton Sorokin. All rights reserved.
//

import Foundation

enum NetworkRequestError: Error {
    case api(_ status: Int, _ code: SGApiResultCode, _ description: String)
    case unknown(Data?, URLResponse?)
}

enum SGApiResultCode: Equatable {
    case kOKResponseCode
    case kBadRequestResponseCode
    case kForbiddenResponseCode
    case kNotFoundResponseCode
    case kTooManyRequestsResponseCode
    case unknown(String)
}

extension SGApiResultCode {
    static func code(for string: String) -> SGApiResultCode {
        switch string {
        case "OK"                : return .kOKResponseCode
        case "Bad Request"       : return .kBadRequestResponseCode
        case "Forbidden"         : return .kForbiddenResponseCode
        case "Not Found"         : return .kNotFoundResponseCode
        case "Too Many Requests" : return .kTooManyRequestsResponseCode
        default                  : return .unknown(string)
        }
    }
}

// protocol describe interface of network model, also used for testing
protocol SGNetwokModelProtocol {
    func fetchGifs(searchTerm: String, withOffset:Int, searchState:SGSearchState, completion: @escaping (Result<[SGGIF], Error>) -> ())
}

/// class that handles network operations
final class SGNetwokModel:SGNetwokModelProtocol {
    private var searchTerm = ""
    private var searchState = SGSearchState.trending;
    
    /// Function to fetch gif information from giphy endpoints
    /// - Parameters:
    ///   - searchTerm: Search query term or phrase.
    ///   - withOffset: Specifies the starting position of the results.
    ///   - searchState: need to create coorect request to
    ///   - completion: A block called when all operation has been completed.
    /// - Returns: Result<[SGGIF], Error> -- if success -- array of SGGIF objects, if failure returns error
    func fetchGifs(searchTerm: String, withOffset:Int, searchState:SGSearchState, completion: @escaping (Result<[SGGIF], Error>) -> ()) {
        
        // if offset == 0 we store search term and search state for future pagination
        // else re set them
        if withOffset == 0 {
            self.searchTerm = searchTerm
            self.searchState = searchState
        }
        
        // create request depending on search term, search state and offset
        let request = SGURLRequestConstructor(searchTerm: self.searchTerm, searchState: self.searchState, offset: withOffset).request()
        // create data task executir, based on request
        let dataTaskExecutor: SGDataTaskExecuterProtocol = SGDataTaskExecuter(request: request)
        
        // execute request
        dataTaskExecutor.fetchGifsRequest { (result) in
            switch result {
            // if execution of the request failed we call completion block and
            // pass error for futher handling
            case .failure(let error):
                completion(.failure(error))
            // if request was sucessful we try to parse recieved data
            case .success(let data):
                ///create SGJSONDataDecoder and pass recieved data
                let decoder = SGJSONDataDecoder(data: data)
                decoder.decodeJSONDataToResponseObject { (result) in
                    switch result {
                    // if parsing was successful we we call completion block
                    // and pass array of gif objects
                    case .success(let gifArray):
                        completion(.success(gifArray))
                    // if parsing was failed we call completion block and
                    // pass error for futher handling
                    case .failure(let error):
                        completion(.failure(error))
                    }
                }
            }
        }
    }
}



