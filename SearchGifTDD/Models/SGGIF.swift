//
//  SGGIF.swift
//  SearchGifTDD
//
//  Created by Anton Sorokin on 11.03.2021.
//  Copyright © 2021 Anton Sorokin. All rights reserved.
//

import UIKit

/// GIF Objects are returned from most of GIPHY API's Endpoints.
/// These objects contain a variety of information, such as the Image Object, which itself includes the URLS
/// for multiple different GIFS formats and sizes.
struct SGGIF: Decodable {
    // An object containing data for various available formats and sizes of this GIF.
    var gifSources: GifImages
    enum CodingKeys: String, CodingKey {
        case gifSources = "images"
    }
    
    /// Function returns gif preview gif url
    /// - Returns: gif preview gif url, if preview gif url is missed or empty, return gifOriginURL()
    func gifPreviewURL() -> String {
        return (gifSources.previewGif.url ?? "").isEmpty ? gifOriginURL() : gifSources.previewGif.url!
    }
    
    /// Function return direct URL for this GIF
    /// - Returns: The publicly-accessible direct URL for this GIF
    func gifOriginURL() -> String {
        return gifSources.original.url
    }
    
    /// Function return URL for this GIF in .MP4 format
    /// - Returns: The URL for this GIF in .MP4 format
    func gifMp4URL() -> String {
        return gifSources.original.mp4
    }
    
    /// Function return height of this GIF in pixels
    /// - Returns: The height of this GIF in pixels.
    func gifHeight() -> CGFloat {
        return CGFloat((gifSources.original.height as NSString).floatValue)
    }
    
    /// Function return height of this GIF in pixels
    /// - Returns: The height of this GIF in pixels
    func gifWidth() -> CGFloat {
        return CGFloat((gifSources.original.width as NSString).floatValue)
    }
}

/// The Images Object found in the GIF Object contains a series of Rendition Objects.
/// These Rendition Objects includes the URLs and sizes for the many different renditions we offer for each GIF.
struct GifImages: Decodable {
    var original: Original
    var previewGif : PreviewGif
    enum CodingKeys: String, CodingKey {
        case original = "original"
        case previewGif = "preview_gif"
    }
}

/// Data on the original version of this GIF.
struct Original: Decodable {
    /// direct URL for this GIF
    var url: String
    /// The URL for this GIF in .MP4 format
    var mp4 : String
    /// The height of this GIF in pixels
    var height: String
    /// The width of this GIF in pixels
    var width: String
}

/// Data on a version of this GIF limited to 50kb that displays the first 1-2 seconds of the GIF.
struct PreviewGif: Decodable {
    /// The URL for this preview GIF
    var url: String?
    
    init(url: String?) {
        self.url = url
    }
    
    enum PreviewGifCodingKeys: String, CodingKey {
        case url = "url"
    }
    
    init(from decoder: Decoder) throws {
        var url: String?
        
        let container = try decoder.container(keyedBy: PreviewGifCodingKeys.self)
        do {
            //if found then map
            url = try container.decode(String.self, forKey: .url)
        }
        catch {
            //not found then just set the default value
            url = ""
        }
        self.init(url: url)
    }
}
