//
//  SGURLRequestConstructor.swift
//  SearchGifTDD
//
//  Created by Anton Sorokin on 23.03.2021.
//  Copyright © 2021 Anton Sorokin. All rights reserved.
//

import Foundation

/// This class encapsulate search term, search state and starting position of the search,
/// and used to create request for SGDataTaskExecuter
class SGURLRequestConstructor {
    private var searchTerm: String
    private var searchState: SGSearchState
    /// starting position of the results.
    private var offset: Int
    
    init(searchTerm: String, searchState: SGSearchState, offset: Int) {
        self.searchTerm = searchTerm
        self.searchState = searchState
        self.offset = offset
    }
    
    /// Function to build URLRequest
    /// - Returns: URLRequest based on searchTerm, searchState and offset
    func request() -> URLRequest {
        switch self.searchState {
        case.search:
            return URLRequest(url: URL.searchURL(searchTerm: self.searchTerm, offset: offset))
        case .trending:
            return URLRequest(url: URL.trendingURL(offset: offset))
        }
    }
}
