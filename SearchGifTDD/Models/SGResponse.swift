//
//  SGResponse.swift
//  SearchGifTDD
//
//  Created by Anton Sorokin on 11.03.2021.
//  Copyright © 2021 Anton Sorokin. All rights reserved.
//

import Foundation

/// Structure describe response of a giphy Api
struct SGResponse: Decodable {
    // Gif information
    var gifs: [SGGIF]
    // meta information
    var meta: SGMeta
    
    enum CodingKeys: String, CodingKey {
        case gifs = "data"
        case meta = "meta"
    }
}
