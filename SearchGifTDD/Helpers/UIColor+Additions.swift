//
//  UIColor+Additions.swift
//  SearchGifTDD
//
//  Created by Anton Sorokin on 17.03.2021.
//  Copyright © 2021 Anton Sorokin. All rights reserved.
//

import UIKit.UIColor

extension UIColor {
    static func randomColor() -> UIColor {
        return UIColor(
            red:   .randomCGFloat(),
            green: .randomCGFloat(),
            blue:  .randomCGFloat(),
            alpha: 1.0
        )
    }
}
