//
//  UICollectionView+Additions.swift
//  SearchGifTDD
//
//  Created by Anton Sorokin on 17.03.2021.
//  Copyright © 2021 Anton Sorokin. All rights reserved.
//

import UIKit.UICollectionView

extension UICollectionView {
    /// Function update collection view's background view with @message
    /// - Parameter message: string to put in UILabel into the collection view's background view
    func setEmptyMessage(_ message: String?) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont.systemFont(ofSize: 15.0)
        messageLabel.sizeToFit()
        
        self.backgroundView = messageLabel;
    }
    
    func hideEmptyMessageIfNeeded() {
        if (self.backgroundView != nil) {
            self.backgroundView = nil
        }
    }
    
    /// Function update collection view's background view with info about error
    /// - Parameter error: some error, recieved after network request or parsing recieved data
    func updateCollectionViewWithError (error: Error) {
        let message = NSLocalizedString("COLLECTION_VIEW_ERROR_TITLE", comment: "COLLECTION_VIEW_ERROR_TITLE") + "\n" + error.localizedDescription
        
        self.setEmptyMessage(message)
    }
    
    /// Function to scroll collection view to top
    func scrollCollectionViewToTop() {
        self.scrollToItem(at: NSIndexPath(item: 0, section: 0) as IndexPath,
                          at: .top,
                          animated: true)
    }
}
