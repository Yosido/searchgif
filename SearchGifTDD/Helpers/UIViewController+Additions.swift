//
//  UIViewController+Additions.swift
//  SearchGifTDD
//
//  Created by Anton Sorokin on 22.03.2021.
//  Copyright © 2021 Anton Sorokin. All rights reserved.
//

import UIKit

import AVKit.AVPlayerViewController

/// class that override viewWillDisappear for AVPlayerViewController to clean up memory
private class SGVideoPlayer: AVPlayerViewController {
    
    typealias DissmissBlock = () -> Void
    var onDismiss: DissmissBlock?
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if isBeingDismissed {
            onDismiss?()
            self.player?.replaceCurrentItem(with: nil)
            self.player = nil
            NotificationCenter.default.removeObserver(self)
        }
    }
}

extension UIViewController {
    /// Function create SGVideoPlayer, set player for them, add observer for ending plaing AVPlayer's item,
    /// and present SGVideoPlayer
    /// - Parameter url: url of mp4 file to play
    func presentVideoPlayerWithURL(url: URL?) {
        if let url = url {
            let playerViewController = SGVideoPlayer()
            // AVAsset can be preloaded using `asset.loadValuesAsynchronously`.
            let originalAsset = AVAsset(url: url)
            // AVPlayerItem is persisted across multiple AVPlayer instances.
            // This is not necessary if you can persist the AVAsset instead (example below).
            let playerItem = AVPlayerItem(asset: originalAsset)
            // Initial preload.
            let player = AVPlayer(playerItem: playerItem)
            
            playerViewController.player = player
            
            NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: player.currentItem, queue: nil) { [weak player] (_) in
                player?.seek(to: CMTime.zero)
                player?.play() }
            
            self.present(playerViewController, animated: true) {
                guard let player = playerViewController.player else {
                    return
                }
                player.play()
            }
        }
    }
}
