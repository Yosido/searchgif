//
//  CGFloat+Additions.swift
//  SearchGifTDD
//
//  Created by Anton Sorokin on 17.03.2021.
//  Copyright © 2021 Anton Sorokin. All rights reserved.
//

import UIKit

extension CGFloat {
    static func randomCGFloat() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}
