//
//  URL+Additions.swift
//  SearchGifTDD
//
//  Created by Anton Sorokin on 17.03.2021.
//  Copyright © 2021 Anton Sorokin. All rights reserved.
//

import Foundation

extension URL {
    /// Function construct URL to call Giphy search endpoint
    /// - Parameters:
    ///   - searchTerm: search term from user input
    ///   - offset: starting position of the results.
    /// - Returns: return URL  to call Giphy search endpoint
    static func searchURL(searchTerm: String, offset: Int) -> URL {
        var components = URLComponents()
        components.scheme = "https"
        components.host = "api.giphy.com"
        components.path = "/v1/gifs/search"
        components.queryItems = [
            URLQueryItem(name: "api_key", value: SGGiphyApiConstanst.kApiKey),
            URLQueryItem(name: "q", value: searchTerm),
            URLQueryItem(name: "limit", value: "10"),
            URLQueryItem(name: "offset", value: String(offset))
        ]
        return components.url!
    }
    
    /// Function construct URL to call Giphy trending endpoint
    /// - Parameter offset: starting position of the results.
    /// - Returns: return URL  to call Giphy trending endpoint
    static func trendingURL(offset: Int) -> URL {
        var components = URLComponents()
        components.scheme = "https"
        components.host = "api.giphy.com"
        components.path = "/v1/gifs/trending"
        components.queryItems = [
            URLQueryItem(name: "api_key", value: SGGiphyApiConstanst.kApiKey),
            URLQueryItem(name: "limit", value: "10"),
            URLQueryItem(name: "offset", value: String(offset))
        ]
        return components.url!
    }
}
