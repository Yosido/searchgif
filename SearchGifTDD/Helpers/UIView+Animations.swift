//
//  UIView+Animations.swift
//  SearchGifTDD
//
//  Created by Anton Sorokin on 24.03.2021.
//  Copyright © 2021 Anton Sorokin. All rights reserved.
//

import UIKit

extension UIView {
    /// Function for zoom in zoom out animation for UIView
    /// - Parameter completion: in completion block we pass BOOL, that describe is animation finished
    func startZoomInOutAnimation(completion: ((Bool) -> Void)?) {
        self.transform = CGAffineTransform(scaleX: 1.6, y: 1.6)
        
        UIView.animate(withDuration: 2.0,
                       delay: 0,
                       usingSpringWithDamping: 0.20,
                       initialSpringVelocity: 6.0,
                       options: UIView.AnimationOptions.allowUserInteraction,
                       animations: { [self] in
                        self.transform = CGAffineTransform.identity
                       },
                       completion: { isFinished in
                        completion?(isFinished)
                       })
        
    }
    
    /// Function for zoom in zoom out animation for UIView
    /// - Parameter completion: in completion block we pass BOOL, that describe is animation finished
    func startFlipAnimation(completion: ((Bool) -> Void)?) {
        self.transform = CGAffineTransform(scaleX: -1, y: 1)
        UIView.animate(withDuration: 0.5,
                       delay: 0.0,
                       usingSpringWithDamping: 0.20,
                       initialSpringVelocity: 6.0,
                       options: [.curveEaseInOut, .transitionFlipFromLeft],
                       animations: { [self] in
                        self.transform = CGAffineTransform.identity
                       },
                       completion:{ isFinished in
                        completion?(isFinished)
                       })
    }
}
