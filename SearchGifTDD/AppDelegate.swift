//
//  AppDelegate.swift
//  SearchGifTDD
//
//  Created by Anton Sorokin on 08.03.2021.
//  Copyright © 2021 Anton Sorokin. All rights reserved.
//

import UIKit
import SDWebImage

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        SDImageCache.shared.config.shouldCacheImagesInMemory = false
                
        let storyboard: UIStoryboard? = UIStoryboard(name: "Main", bundle: nil)

        guard let searchGifViewCountroller = storyboard?.instantiateViewController(identifier: "SGSearchGifViewController", creator: { coder in
            return SGSearchGifViewController(coder: coder)
        }) else {
            fatalError("Failed to initialize view controller")
        }

        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = searchGifViewCountroller
        self.window?.makeKeyAndVisible()
        
        return true
    }
}

