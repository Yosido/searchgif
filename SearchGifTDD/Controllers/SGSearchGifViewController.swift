//
//  SGSearchGifViewController.swift
//  SearchGifTDD
//
//  Created by Anton Sorokin on 08.03.2021.
//  Copyright © 2021 Anton Sorokin. All rights reserved.
//

import UIKit

class SGSearchGifViewController: UIViewController {
    @IBOutlet weak var resultCollectionView: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    private let networkModel: SGNetwokModelProtocol
    private(set) var searchState: SGSearchState = .trending
    
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var footerHeightConstraint: NSLayoutConstraint!
    private(set) var dataSource:SGGifDataSource
    
    init?(coder: NSCoder, networkModel: SGNetwokModelProtocol = SGNetwokModel(), dataSource: SGGifDataSource  = SGGifDataSource(gifs: [])) {
        self.networkModel = networkModel
        self.dataSource = dataSource
        super.init(coder: coder)
    }
    
    required init?(coder: NSCoder) {
        self.networkModel = SGNetwokModel()
        self.dataSource = SGGifDataSource(gifs: [])
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSearchBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupCollectionView()
        
        if resultCollectionView.numberOfItems(inSection: 0) == 0 {
            refresh()
        }
    }
    
    private func refresh() {
        networkModel.fetchGifs(searchTerm: "", withOffset: 0, searchState: searchState) { (result) in
            switch result {
            case .failure(let error):
                //clear data source
                self.dataSource.clearGifArray()
                DispatchQueue.main.async {
                    self.resultCollectionView.updateCollectionViewWithError(error: error)
                }
            ///update cillectionView
            case .success(let gifArray):
                /// update data source
                if (gifArray.count > 0) {
                    self.dataSource.appendGifArray(data: gifArray)
                    DispatchQueue.main.async {
                        self.resultCollectionView.reloadData()
                    }
                } else {
                    self.dataSource.clearGifArray()
                }
            }
        }
    }
    
    fileprivate func setupCollectionView() {
        self.resultCollectionView.translatesAutoresizingMaskIntoConstraints = false
        
        let nib = UINib(nibName: "SGGIFCollectionViewCell", bundle: nil)
        self.resultCollectionView.register(nib, forCellWithReuseIdentifier: "SGGIFCollectionViewCellIdentifier")
        
        self.resultCollectionView.contentInset = .zero
        self.resultCollectionView.delegate = self
        
        self.resultCollectionView.setCollectionViewLayout(SGGridLayout(), animated: true)
        if let layout = resultCollectionView?.collectionViewLayout as? SGGridLayout {
            layout.delegate = self
        }
        
        dataSource.cellDelegate = self
        resultCollectionView.dataSource = dataSource
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        guard previousTraitCollection != nil else { return }
        guard let collectionViewLayout = resultCollectionView.collectionViewLayout as? SGGridLayout else {
            return
        }
        collectionViewLayout.updateLayout()
        collectionViewLayout.invalidateLayout()
    }
    
    private func setupSearchBar() -> Void {
        self.searchBar.backgroundImage = UIImage()
        searchBar.delegate = self
    }
    
    func showFooter() {
        footerHeightConstraint.constant = SGViewConstants.kFooterHeight
        footerView.addSubview(createSpinnerFooter())
    }
    
    func hideFooter() {
        footerHeightConstraint.constant = 0
        for v in footerView.subviews{
           v.removeFromSuperview()
        }
    }
    
    func createSpinnerFooter() -> UIView {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: SGViewConstants.kFooterHeight))
        
        let spinner = UIActivityIndicatorView()
        spinner.center = footerView.center
        footerView.addSubview(spinner)
        spinner.startAnimating()
        return footerView
    }
    
    func updateNextSet() {
        networkModel.fetchGifs(searchTerm: "", withOffset: dataSource.getGifArrayCount(), searchState: searchState) { [weak self] (result) in
            DispatchQueue.main.async {
                self?.hideFooter()
            }
            switch result {
            case .failure(let error):
                //clear data source
                self?.dataSource.clearGifArray()
                DispatchQueue.main.async {
                    self?.resultCollectionView.reloadData()
                    self?.resultCollectionView.updateCollectionViewWithError(error: error)
                }
            ///update collectionView
            case .success(let gifArray):
                /// update data source only if we get non empty to avoid permanent updating of collection view
                if (gifArray.count > 0) {
                    self?.dataSource.appendGifArray(data: gifArray)
                    DispatchQueue.main.async {
                        self?.resultCollectionView.reloadData()
                    }
                }
            }
        }
    }
}

// MARK: - UICollectionViewDelegate

extension SGSearchGifViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        let gif = dataSource.getGifByIndex(index: indexPath.row)
        let videoURL = URL(string: gif.gifMp4URL())
        self.presentVideoPlayerWithURL(url: videoURL)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == dataSource.getGifArrayCount() - 1 {
            showFooter()
            updateNextSet()
        }
    }
}

// MARK: - SGGridLayoutDelegate
extension SGSearchGifViewController: SGGridLayoutDelegate {
    func collectionView(
        _ collectionView: UICollectionView,
        heightForGifAtIndexPath indexPath:IndexPath) -> CGFloat {
        
        let scaleFactor = dataSource.getGifByIndex(index: indexPath.row).gifWidth() / collectionView.frame.size.width * 2
        let gifHeight = (dataSource.getGifByIndex(index: indexPath.row).gifHeight() / scaleFactor).rounded()
        return gifHeight
    }
}

// MARK: - UISearchBarDelegate
extension SGSearchGifViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if (searchBar.searchTextField.text == "") {
            self.searchBar.resignFirstResponder()
            self.searchState = .trending
        } else {
            self.searchState = .search
        }
        
        networkModel.fetchGifs(searchTerm: searchText, withOffset: 0, searchState: searchState) { [weak self] (result) in
            switch result {
            case .failure(let error):
                //clear data source
                self?.dataSource.clearGifArray()
                DispatchQueue.main.async {
                    self?.resultCollectionView.reloadData()
                    self?.resultCollectionView.updateCollectionViewWithError(error: error)
                }
            ///update cillectionView
            case .success(let gifArray):
                /// update data source
                self?.dataSource.clearGifArray()
                self?.dataSource.appendGifArray(data: gifArray)
                guard let gifArrayCount = self?.dataSource.getGifArrayCount() else { return }
                if (gifArrayCount > 0) {
                    DispatchQueue.main.async {
                        if let layout = self?.resultCollectionView?.collectionViewLayout as? SGGridLayout {
                            layout.updateLayout()
                        }
                        self?.resultCollectionView.reloadData()
                        self?.resultCollectionView.scrollCollectionViewToTop()
                    }
                } else {
                    self?.dataSource.clearGifArray()
                    DispatchQueue.main.async {
                        self?.resultCollectionView.reloadData()
                    }
                }
            }
        }
    }
}

// MARK: - SGGIFCollectionViewCellDelegate

extension SGSearchGifViewController: SGGIFCollectionViewCellDelegate {
    func shareGifButtonPressed(gifOriginURL: String?, gifImageData: Data?, sourceView: UIView) {
        guard let shareData = gifImageData else {
            return
        }
        
        let firstActivityItem: Array = [shareData]
        let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: firstActivityItem, applicationActivities: nil)

        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [.message, .mail, .addToReadingList, .postToVimeo]

        activityViewController.popoverPresentationController?.permittedArrowDirections = .any
        activityViewController.modalPresentationStyle = UIModalPresentationStyle.popover
        activityViewController.popoverPresentationController?.sourceView = sourceView
        activityViewController.popoverPresentationController?.sourceRect = sourceView.bounds
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }

    
    func copyGifURLButtonPressed(gifOriginURL: String?) {
        UIPasteboard.general.string = gifOriginURL
    }
}

