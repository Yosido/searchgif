//
//  SGGifDataSource.swift
//  SearchGifTDD
//
//  Created by Anton Sorokin on 11.03.2021.
//  Copyright © 2021 Anton Sorokin. All rights reserved.
//

import UIKit

class SGGifDataSource: NSObject, UICollectionViewDataSource {
    private let dataProviderQueue = DispatchQueue(label: "com.sg.SGGifDataProviderQueue", attributes: .concurrent, target: nil)
    
    weak var cellDelegate: SGGIFCollectionViewCellDelegate?
    private var unsafeGifArray: [SGGIF] = []
    private var gifArray: [SGGIF] {
        get {
            var gifArrayCopy: [SGGIF]!
            dataProviderQueue.sync {
                gifArrayCopy = self.unsafeGifArray
            }
            return gifArrayCopy
        }
    }
    
    init(gifs: [SGGIF]) {
        self.unsafeGifArray = gifs
        super.init()
    }
    
    func appendGifArray(data: [SGGIF]) -> Void {
        dataProviderQueue.async(flags: .barrier) { [weak self] in
            guard let self = self else {
                return
            }
            self.unsafeGifArray.append(contentsOf: data)
        }
    }
    
    func getGifArrayCount() -> Int {
        return gifArray.count
    }
    
    func getGifByIndex(index: Int) -> SGGIF {
        return gifArray[index]
    }
    
    func clearGifArray() -> Void {
        self.unsafeGifArray.removeAll()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (getGifArrayCount() == 0) {
            collectionView.setEmptyMessage(NSLocalizedString("COLLECTION_VIEW_EMPTY_MESSAGE", comment: "COLLECTION_VIEW_EMPTY_MESSAGE"))
        } else {
            collectionView.hideEmptyMessageIfNeeded()
        }
        
        return gifArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cellIdentifier = "SGGIFCollectionViewCellIdentifier"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! SGGIFCollectionViewCell
        
        cell.configureCellWithGifObject(gif: gifArray[indexPath.row])
        cell.delegate = cellDelegate
        
        return cell
    }
}
