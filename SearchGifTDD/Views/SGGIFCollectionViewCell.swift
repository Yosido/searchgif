//
//  SGGIFCollectionViewCell.swift
//  ShareGif
//
//  Created by Anton Sorokin on 11.03.2021.
//  Copyright © 2020 Anton Sorokin. All rights reserved.
//

import UIKit
import SDWebImage

protocol SGGIFCollectionViewCellDelegate: AnyObject {
    func shareGifButtonPressed(gifOriginURL: String?, gifImageData: Data?, sourceView: UIView)
    func copyGifURLButtonPressed(gifOriginURL: String?)
}

class SGGIFCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak private var cellBackgroundImageView: SDAnimatedImageView!
    @IBOutlet weak private var shareButton: UIButton!
    @IBOutlet weak var copyGifURLButton: UIButton!
    
    weak var delegate: SGGIFCollectionViewCellDelegate?
    private(set) var gifObject: SGGIF?
    var gifDataToShare: Data?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cellBackgroundImageView.layer.cornerRadius = SGViewConstants.kSGCollectionViewCellCornerRadius
        self.backgroundColor = UIColor(named: "collectionViewBgColor")
    }
    
    override func prepareForReuse() {
        cellBackgroundImageView.image = nil
        gifDataToShare = nil
    }
    
    public func configureCellWithGifObject(gif: SGGIF) {
        self.gifObject = gif
        cellBackgroundImageView.sd_setImage(with: URL(string: gif.gifPreviewURL()), placeholderImage: UIImage.imageWithRandomColor(), options: [.progressiveLoad]) { [weak self] image, _, _, imageUrl in
            self?.gifDataToShare = image?.sd_imageData(as: .GIF)
        }
        
        
    }
    
    func startShareButtonAnimation(completion: ((Bool) -> Void)?) {
        shareButton.startZoomInOutAnimation { (isFinished) in
            completion?(isFinished)
        }
    }
    
    func startCopyGifURLButtonAnimation(completion: ((Bool) -> Void)?) {
        copyGifURLButton.startFlipAnimation { (isFinished) in
            completion?(isFinished)
        }
    }
    
    @IBAction func shareGifButtonPressed(_ sender: Any) {
        startShareButtonAnimation { (isFinished) in }
        delegate?.shareGifButtonPressed(gifOriginURL: gifObject?.gifOriginURL(), gifImageData: gifDataToShare, sourceView: self.shareButton)
    }
    
    @IBAction func copyGifURLButtonPressed(_ sender: Any) {
        startCopyGifURLButtonAnimation { (isFinished) in }
        delegate?.copyGifURLButtonPressed(gifOriginURL: gifObject?.gifOriginURL())
    }
}
