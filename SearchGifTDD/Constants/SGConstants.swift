//
//  SGConstants.swift
//  SearchGifTDD
//
//  Created by Anton Sorokin on 11.03.2021.
//  Copyright © 2021 Anton Sorokin. All rights reserved.
//

import UIKit

enum SGResponseCode {
    static let kOKResponseCode: Int = 200
    static let kBadRequestResponseCode: Int = 400
    static let kForbiddenResponseCode: Int = 403
    static let kNotFoundResponseCode: Int = 404
    static let kTooManyRequestsResponseCode: Int = 429
}

enum SGSearchState: Int {
    case trending, search
}

enum SGViewConstants {
    static let kSGCollectionViewCellCornerRadius: CGFloat = 12
    static let kFooterHeight: CGFloat = 100
}

enum SGGiphyApiConstanst {
    static let kApiKey = "deokzgUjxm6QHQdp3H3aca1LSZcCpucc"
    //let apiKey = "dc6zaTOxFJmzC"
}

enum PresentationStyle: String, CaseIterable {
    case table
    case defaultGrid
}
