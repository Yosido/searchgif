//
//  SGGIFTests.swift
//  SearchGifTDDTests
//
//  Created by Anton Sorokin on 11.03.2021.
//  Copyright © 2021 Anton Sorokin. All rights reserved.
//

import XCTest
@testable import SearchGifTDD

class SGGIFTests: XCTestCase {
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testGifCreation() -> Void {
        let testGif = SGGIF(gifSources: GifImages(original: Original(url: "url", mp4: "mp4", height: "100", width: "100"), previewGif: PreviewGif(url: "previewURL")))
        XCTAssertTrue(testGif.gifOriginURL() == "url")
    }
    
    func testPreviewURL() {
        var testGif = SGGIF(gifSources: GifImages(original: Original(url: "url", mp4: "mp4", height: "100", width: "100"), previewGif: PreviewGif(url: "previewURL")))
        XCTAssertTrue(testGif.gifPreviewURL() == "previewURL")
        testGif.gifSources.previewGif.url = nil
        XCTAssertTrue(testGif.gifPreviewURL() == "url")
    }
    
    func testMp4URL() -> Void {
        let testGif = SGGIF(gifSources: GifImages(original: Original(url: "url", mp4: "mp4url", height: "100", width: "100"), previewGif: PreviewGif(url: "previewURL")))
        XCTAssertTrue(testGif.gifMp4URL() == "mp4url")
    }
    
    func testGifWidth() -> Void {
        let testGif = SGGIF(gifSources: GifImages(original: Original(url: "url", mp4: "mp4url", height: "101", width: "100"), previewGif: PreviewGif(url: "previewURL")))
        XCTAssertEqual(testGif.gifWidth(), 100)
    }
    
    func testGifHeight() -> Void {
        let testGif = SGGIF(gifSources: GifImages(original: Original(url: "url", mp4: "mp4url", height: "101", width: "100"), previewGif: PreviewGif(url: "previewURL")))
        XCTAssertEqual(testGif.gifHeight(), 101)
    }
}
