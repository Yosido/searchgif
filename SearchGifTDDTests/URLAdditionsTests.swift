//
//  URLAdditionsTests.swift
//  SearchGifTDDTests
//
//  Created by Anton Sorokin on 18.03.2021.
//  Copyright © 2021 Anton Sorokin. All rights reserved.
//

import XCTest
@testable import SearchGifTDD

class URLAdditionsTests: XCTestCase {
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testSearchURL() {
        let url = URL.searchURL(searchTerm: "term", offset: 0)
        
        XCTAssertNotNil(url)
        XCTAssertEqual(url.absoluteString, "https://api.giphy.com/v1/gifs/search?api_key=deokzgUjxm6QHQdp3H3aca1LSZcCpucc&q=term&limit=10&offset=0")
    }
    
    func testTrendingURL() {
        let url = URL.trendingURL(offset: 0)
        XCTAssertNotNil(url)
        XCTAssertEqual(url.absoluteString, "https://api.giphy.com/v1/gifs/trending?api_key=deokzgUjxm6QHQdp3H3aca1LSZcCpucc&limit=10&offset=0")
    }
}
