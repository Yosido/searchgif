//
//  XCTestCase+Additions.swift
//  SearchGifTDDTests
//
//  Created by Anton Sorokin on 26.04.2022.
//

@testable import SearchGifTDD
import XCTest

extension XCTestCase {
    func createSGSearchGifViewController(networkModel: SGNetwokModelProtocol = SGNetworkModelSpy(),
                             gifs: [SGGIF] = []) -> SGSearchGifViewController? {
        
        let storyboard: UIStoryboard? = UIStoryboard(name: "Main", bundle: nil)

        guard let sut = storyboard?.instantiateViewController(identifier: "SGSearchGifViewController", creator: { coder in
            return SGSearchGifViewController(coder: coder,
                                             networkModel: networkModel,
                                             dataSource: SGGifDataSource(gifs: gifs))
        }) else {
            fatalError("Failed to initialize view controller")
        }
        return sut
    }
    
    func createSGSearchGifViewControllerWithoutAnimations(networkModel: SGNetwokModelProtocol = SGNetworkModelSpy(),
                             gifs: [SGGIF] = []) -> TestableSGSearchGifViewController? {
        
        let storyboard: UIStoryboard? = UIStoryboard(name: "Main", bundle: nil)

        guard let sut = storyboard?.instantiateViewController(identifier: "SGSearchGifViewController", creator: { coder in
            return TestableSGSearchGifViewController(coder: coder,
                                             networkModel: networkModel,
                                             dataSource: SGGifDataSource(gifs: gifs))
        }) else {
            fatalError("Failed to initialize view controller")
        }
        return sut
    }
}

final class TestableSGSearchGifViewController: SGSearchGifViewController {
    var presentedVC: UIViewController?
    
    override func present(_ vc: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        presentedVC = vc
    }
}
