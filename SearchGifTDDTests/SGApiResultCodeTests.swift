//
//  SGApiResultCodeTests.swift
//  SearchGifTDDTests
//
//  Created by Anton Sorokin on 23.03.2021.
//  Copyright © 2021 Anton Sorokin. All rights reserved.
//

import XCTest
@testable import SearchGifTDD

class SGApiResultCodeTests: XCTestCase {
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testApiResultCode() {
        let inputStrings = ["OK",
                            "Bad Request",
                            "Forbidden",
                            "Not Found",
                            "Too Many Requests",
                            "unknown code"]
        let results:[SGApiResultCode] = [.kOKResponseCode,
                                         .kBadRequestResponseCode,
                                         .kForbiddenResponseCode,
                                         .kNotFoundResponseCode,
                                         .kTooManyRequestsResponseCode,
                                         .unknown("unknown code")]
        
        for index in 0..<inputStrings.count {
            let result = SGApiResultCode.code(for: inputStrings[index]) == results[index]
            XCTAssertTrue(result)
        }
    }
}
