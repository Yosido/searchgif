//
//  SGDataTaskExecuterSpy.swift
//  SearchGifTDDTests
//
//  Created by Anton Sorokin on 27.04.2022.
//

@testable import SearchGifTDD
import Foundation

class SGDataTaskExecuterSpy: SGDataTaskExecuterProtocol {
    
    private var results: [Result<Data,Error>]
    
    init(result:Data = Data()) {
        self.results = [.success(result)]
    }
    
    init(results:[Result<Data,Error>]) {
        self.results = results
    }
    
    func fetchGifsRequest(completion: @escaping (Result<Data, Error>) -> ()) {
        completion(results.removeFirst())
    }
}
