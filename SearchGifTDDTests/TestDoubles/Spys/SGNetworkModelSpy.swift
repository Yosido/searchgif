//
//  SGNetworkModelSpy.swift
//  SearchGifTDDTests
//
//  Created by Anton Sorokin on 20.04.2022.
//

@testable import SearchGifTDD

final class SGNetworkModelSpy: SGNetwokModelProtocol {
    private (set) var loadGifsCallCount = 0
    private var results: [Result<[SGGIF],Error>]
    
    init(result:[SGGIF] = []) {
        self.results = [.success(result)]
    }
    
    init(results:[Result<[SGGIF],Error>]) {
        self.results = results
    }
    
    func fetchGifs(searchTerm: String, withOffset: Int, searchState: SGSearchState, completion: @escaping (Result<[SGGIF], Error>) -> ()) {
        loadGifsCallCount += 1
        completion(results.removeFirst())
    }
}
