//
//  SGGIFCollectionViewCellDelegateTests.swift
//  SearchGifTDDTests
//
//  Created by Anton Sorokin on 12.03.2021.
//  Copyright © 2021 Anton Sorokin. All rights reserved.
//

import XCTest
@testable import SearchGifTDD

class SGGIFCollectionViewCellDelegateTests: XCTestCase {
    func test_displayActivityViewController_afterShareButtonPressed() {
        let gifs: [SGGIF] = GifsStub.createCorrectGifsStub()
        
        let networkModel: SGNetworkModelSpy = SGNetworkModelSpy(results: [
            .success(gifs)
        ])
        
        guard let vc = createSGSearchGifViewControllerWithoutAnimations(networkModel: networkModel) else { return }
        
        vc.simulateViewWillAppear()
        
        guard let sut = vc.gifCell(at: 0) else { return }
        
        vc.dataSource.cellDelegate?.shareGifButtonPressed(gifOriginURL: "url", gifImageData: GifsStub.createGifData(), sourceView: sut)
        
        let activityController = vc.presentedVC as? UIActivityViewController

        
        XCTAssertNotNil(activityController)
    }
    
    func test_copyGifURL_afterCopyURLButtonPressed () {
        UIPasteboard.general.string = ""
        let gifs: [SGGIF] = GifsStub.createCorrectGifsStub()
        
        let networkModel: SGNetworkModelSpy = SGNetworkModelSpy(results: [
            .success(gifs)
        ])
        
        guard let vc = createSGSearchGifViewControllerWithoutAnimations(networkModel: networkModel) else { return }
        
        vc.simulateViewWillAppear()
        
        guard let sut = vc.gifCell(at: 0) else { return }
        
        vc.dataSource.cellDelegate?.copyGifURLButtonPressed(gifOriginURL: sut.gifObject?.gifOriginURL())
        
        XCTAssertEqual(UIPasteboard.general.string, sut.gifObject?.gifOriginURL())
    }
}
