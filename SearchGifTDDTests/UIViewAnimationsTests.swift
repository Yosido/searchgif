//
//  UIViewAnimationsTests.swift
//  SearchGifTDDTests
//
//  Created by Anton Sorokin on 24.03.2021.
//  Copyright © 2021 Anton Sorokin. All rights reserved.
//

import XCTest
@testable import SearchGifTDD

class UIViewAnimationsTests: XCTestCase {
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        super.tearDown()
        RunLoop.current.run(until: Date())
        UIView.setAnimationsEnabled(true)
    }
    
    func testZoomInOutAnimationComplete() {
        let expectation = self.expectation(description: "Expect completion handler to be called.")
        
        var finished = false
        let view = UIView()
        view.alpha = 1.0
        let window = UIWindow()
        window.addSubview(view)
        window.isHidden = false
        
        UIView.setAnimationsEnabled(false)
        
        view.startZoomInOutAnimation { (isFinished) in
            finished = isFinished
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 2.0)
        
        XCTAssertTrue(finished)
    }
    
    func testFlipAnimation() {
        let expectation = self.expectation(description: "Expect completion handler to be called.")
        
        var finished = false
        let view = UIView()
        view.alpha = 1.0
        let window = UIWindow()
        window.addSubview(view)
        window.isHidden = false
        
        UIView.setAnimationsEnabled(false)
        
        view.startFlipAnimation() { (isFinished) in
            finished = isFinished
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 2.0)
        
        XCTAssertTrue(finished)
    }
}
