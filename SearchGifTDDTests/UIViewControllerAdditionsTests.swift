//
//  UIViewControllerAdditionsTests.swift
//  SearchGifTDDTests
//
//  Created by Anton Sorokin on 23.03.2021.
//  Copyright © 2021 Anton Sorokin. All rights reserved.
//

import XCTest
@testable import SearchGifTDD
import AVKit.AVPlayerViewController

class UIViewControllerAdditionsTests: XCTestCase {
    func testPresentVideoPlayer() {
        let gifs: [SGGIF] = GifsStub.createCorrectGifsStub()
        let testUrl = "https://media4.giphy.com/media/3o6Ztp5m2kEyxCo5sA/giphy.mp4"
        
        let networkModel: SGNetworkModelSpy = SGNetworkModelSpy(results: [
            .success(gifs)
        ])
        
        guard let sut = createSGSearchGifViewControllerWithoutAnimations(networkModel: networkModel, gifs: gifs) else { return }
        
        sut.simulateViewWillAppear()
        
        sut.presentVideoPlayerWithURL(url: URL(string: testUrl))
        let playerVC = sut.presentedVC as? AVPlayerViewController
        XCTAssertEqual((playerVC?.player?.currentItem?.asset as? AVURLAsset)?.url.absoluteString, testUrl)
    }
    
    func testVideoPlayerReleaseMemory() {
        let gifs: [SGGIF] = GifsStub.createCorrectGifsStub()
        let testUrl = "https://media4.giphy.com/media/3o6Ztp5m2kEyxCo5sA/giphy.mp4"
        
        let networkModel: SGNetworkModelSpy = SGNetworkModelSpy(results: [
            .success(gifs)
        ])
        
        guard let sut = createSGSearchGifViewControllerWithoutAnimations(networkModel: networkModel, gifs: gifs) else { return }
        
        sut.simulateViewWillAppear()
        
        sut.presentVideoPlayerWithURL(url: URL(string: testUrl))
        let playerVC = sut.presentedVC as? AVPlayerViewController
        playerVC?.parent?.dismiss(animated: false, completion: {
            
            XCTAssertNil(playerVC?.player)
            XCTAssertNil(playerVC?.player?.currentItem)
        })
    }
}
