//
//  SGResponseTests.swift
//  SearchGifTDDTests
//
//  Created by Anton Sorokin on 11.03.2021.
//  Copyright © 2021 Anton Sorokin. All rights reserved.
//

import XCTest
@testable import SearchGifTDD

class SGResponseTests: XCTestCase {
    
    func testCorrectEmptyJSON() throws {
        let data = fixtureCorrectEmptyJSON
        let response = try JSONDecoder().decode(SGResponse.self, from: data)
        
        XCTAssertEqual(response.meta.status, 200)
        XCTAssertTrue(response.gifs.count == 0)
    }
    
    func testCorrectJSON() throws {
        let data = fixtureCorrectJSON
        let response = try JSONDecoder().decode(SGResponse.self, from: data)
        
        XCTAssertEqual(response.meta.status, 200)
        XCTAssertTrue(response.gifs.count == 1)
    }
    
    func testMessedPreviewJSON() {
        
        let data = fixtureMissedPreviewJSON
        
        XCTAssertThrowsError(try JSONDecoder().decode(SGResponse.self, from: data)) { error in
            if case .keyNotFound(let key, _)? = error as? DecodingError {
                XCTAssertEqual("preview_gif", key.stringValue)
            } else {
                XCTFail("Expected '.keyNotFound' but got \(error)")
            }
        }
    }
    
    func testMessedPreviewURL_JSON() throws {
        
        let data = fixtureMissedPreviewURLJSON
        
        let response = try JSONDecoder().decode(SGResponse.self, from: data)
        
        XCTAssertEqual(response.gifs.count, 1)
        
        let gif = response.gifs.first
        
        XCTAssertEqual(gif?.gifPreviewURL(), "https://media4.giphy.com/media/3o6Ztp5m2kEyxCo5sA/giphy.gif?cid=e1bb72ffauam6l1yi6k678ns64m31xheyim52a3r5eyrcn0f&rid=giphy.gif")
    }
}

private let fixtureCorrectEmptyJSON = Data("""
{"data":[],"pagination":{"total_count":0,"count":0,"offset":0},"meta":{"status":200,"msg":"OK","response_id":"mxpdc9mwun58ca4sto5dl0l0aix8dmnab6yisztn"}}
""".utf8)

private let fixtureMissedPreviewJSON = Data("""
{
   "data":[
      {
         "type":"gif",
         "id":"3o6Ztp5m2kEyxCo5sA",
         "url":"https://giphy.com/gifs/southparkgifs-3o6Ztp5m2kEyxCo5sA",
         "slug":"southparkgifs-3o6Ztp5m2kEyxCo5sA",
         "bitly_gif_url":"http://gph.is/2bSKTqA",
         "bitly_url":"http://gph.is/2bSKTqA",
         "embed_url":"https://giphy.com/embed/3o6Ztp5m2kEyxCo5sA",
         "username":"southpark",
         "source":"http://comedycentral.com",
         "title":"scared eric cartman GIF by South Park ",
         "rating":"pg-13",
         "content_url":"",
         "source_tld":"comedycentral.com",
         "source_post_url":"http://comedycentral.com",
         "is_sticker":0,
         "import_datetime":"2016-08-23 18:25:33",
         "trending_datetime":"1970-01-01 00:00:00",
         "images":{
            "original":{
               "height":"270",
               "width":"480",
               "size":"865941",
               "url":"https://media4.giphy.com/media/3o6Ztp5m2kEyxCo5sA/giphy.gif?cid=e1bb72ffauam6l1yi6k678ns64m31xheyim52a3r5eyrcn0f&rid=giphy.gif",
               "mp4_size":"261914",
               "mp4":"https://media4.giphy.com/media/3o6Ztp5m2kEyxCo5sA/giphy.mp4?cid=e1bb72ffauam6l1yi6k678ns64m31xheyim52a3r5eyrcn0f&rid=giphy.mp4",
               "webp_size":"918666",
               "webp":"https://media4.giphy.com/media/3o6Ztp5m2kEyxCo5sA/giphy.webp?cid=e1bb72ffauam6l1yi6k678ns64m31xheyim52a3r5eyrcn0f&rid=giphy.webp",
               "frames":"142",
               "hash":"0a7fc2bde5138b090355525099b7a50b"
            }
         }
      }
   ],
   "pagination":{
      "total_count":1,
      "count":1,
      "offset":0
   },
   "meta":{
      "status":200,
      "msg":"OK",
      "response_id":"auam6l1yi6k678ns64m31xheyim52a3r5eyrcn0f"
   }
}
""".utf8)

private let fixtureMissedPreviewURLJSON = Data("""
{
   "data":[
      {
         "type":"gif",
         "id":"3o6Ztp5m2kEyxCo5sA",
         "url":"https://giphy.com/gifs/southparkgifs-3o6Ztp5m2kEyxCo5sA",
         "slug":"southparkgifs-3o6Ztp5m2kEyxCo5sA",
         "bitly_gif_url":"http://gph.is/2bSKTqA",
         "bitly_url":"http://gph.is/2bSKTqA",
         "embed_url":"https://giphy.com/embed/3o6Ztp5m2kEyxCo5sA",
         "username":"southpark",
         "source":"http://comedycentral.com",
         "title":"scared eric cartman GIF by South Park ",
         "rating":"pg-13",
         "content_url":"",
         "source_tld":"comedycentral.com",
         "source_post_url":"http://comedycentral.com",
         "is_sticker":0,
         "import_datetime":"2016-08-23 18:25:33",
         "trending_datetime":"1970-01-01 00:00:00",
         "images":{
            "original":{
               "height":"270",
               "width":"480",
               "size":"865941",
               "url":"https://media4.giphy.com/media/3o6Ztp5m2kEyxCo5sA/giphy.gif?cid=e1bb72ffauam6l1yi6k678ns64m31xheyim52a3r5eyrcn0f&rid=giphy.gif",
               "mp4_size":"261914",
               "mp4":"https://media4.giphy.com/media/3o6Ztp5m2kEyxCo5sA/giphy.mp4?cid=e1bb72ffauam6l1yi6k678ns64m31xheyim52a3r5eyrcn0f&rid=giphy.mp4",
               "webp_size":"918666",
               "webp":"https://media4.giphy.com/media/3o6Ztp5m2kEyxCo5sA/giphy.webp?cid=e1bb72ffauam6l1yi6k678ns64m31xheyim52a3r5eyrcn0f&rid=giphy.webp",
               "frames":"142",
               "hash":"0a7fc2bde5138b090355525099b7a50b"
            },
            "preview_gif":{
            }
         }
      }
   ],
   "pagination":{
      "total_count":1,
      "count":1,
      "offset":0
   },
   "meta":{
      "status":200,
      "msg":"OK",
      "response_id":"auam6l1yi6k678ns64m31xheyim52a3r5eyrcn0f"
   }
}
""".utf8)

private let fixtureCorrectJSON = Data("""
{
   "data":[
      {
         "type":"gif",
         "id":"3o6Ztp5m2kEyxCo5sA",
         "url":"https://giphy.com/gifs/southparkgifs-3o6Ztp5m2kEyxCo5sA",
         "slug":"southparkgifs-3o6Ztp5m2kEyxCo5sA",
         "bitly_gif_url":"http://gph.is/2bSKTqA",
         "bitly_url":"http://gph.is/2bSKTqA",
         "embed_url":"https://giphy.com/embed/3o6Ztp5m2kEyxCo5sA",
         "username":"southpark",
         "source":"http://comedycentral.com",
         "title":"scared eric cartman GIF by South Park ",
         "rating":"pg-13",
         "content_url":"",
         "source_tld":"comedycentral.com",
         "source_post_url":"http://comedycentral.com",
         "is_sticker":0,
         "import_datetime":"2016-08-23 18:25:33",
         "trending_datetime":"1970-01-01 00:00:00",
         "images":{
            "original":{
               "height":"270",
               "width":"480",
               "size":"865941",
               "url":"https://media4.giphy.com/media/3o6Ztp5m2kEyxCo5sA/giphy.gif?cid=e1bb72ffauam6l1yi6k678ns64m31xheyim52a3r5eyrcn0f&rid=giphy.gif",
               "mp4_size":"261914",
               "mp4":"https://media4.giphy.com/media/3o6Ztp5m2kEyxCo5sA/giphy.mp4?cid=e1bb72ffauam6l1yi6k678ns64m31xheyim52a3r5eyrcn0f&rid=giphy.mp4",
               "webp_size":"918666",
               "webp":"https://media4.giphy.com/media/3o6Ztp5m2kEyxCo5sA/giphy.webp?cid=e1bb72ffauam6l1yi6k678ns64m31xheyim52a3r5eyrcn0f&rid=giphy.webp",
               "frames":"142",
               "hash":"0a7fc2bde5138b090355525099b7a50b"
            },
            "preview_gif":{
               "height":"98",
               "width":"174",
               "size":"46521",
               "url":"https://media4.giphy.com/media/3o6Ztp5m2kEyxCo5sA/giphy-preview.gif?cid=e1bb72ffauam6l1yi6k678ns64m31xheyim52a3r5eyrcn0f&rid=giphy-preview.gif"
            }
         }
      }
   ],
   "pagination":{
      "total_count":1,
      "count":1,
      "offset":0
   },
   "meta":{
      "status":200,
      "msg":"OK",
      "response_id":"auam6l1yi6k678ns64m31xheyim52a3r5eyrcn0f"
   }
}
""".utf8)

