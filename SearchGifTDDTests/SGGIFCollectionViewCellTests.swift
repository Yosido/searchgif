//
//  SGGIFCollectionViewCellTests.swift
//  SearchGifTDDTests
//
//  Created by Anton Sorokin on 24.03.2021.
//  Copyright © 2021 Anton Sorokin. All rights reserved.
//

import XCTest
@testable import SearchGifTDD

class SGGIFCollectionViewCellTests: XCTestCase {
    var dataSource: SGGifDataSource!
    var collectionView: UICollectionView!
    
    override func setUp() {
        let gifs: [SGGIF] = [SGGIF(gifSources: GifImages(original: Original(url: "url1", mp4: "mp4", height: "100", width: "100"), previewGif: PreviewGif(url: "previewURL"))),
                             SGGIF(gifSources: GifImages(original: Original(url: "url2", mp4: "mp4", height: "100", width: "100"), previewGif: PreviewGif(url: "previewURL"))),
                             SGGIF(gifSources: GifImages(original: Original(url: "url3", mp4: "mp4", height: "100", width: "100"), previewGif: PreviewGif(url: "previewURL")))]
        
        dataSource = SGGifDataSource(gifs: gifs)
        
        let layout = UICollectionViewFlowLayout()
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        layout.scrollDirection = .vertical
        
        collectionView.dataSource = dataSource
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testShareButttonAnimationFinish() {
        let nib = UINib(nibName: "SGGIFCollectionViewCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "SGGIFCollectionViewCellIdentifier")
        let cell = dataSource.collectionView(collectionView, cellForItemAt: IndexPath(row: 0, section: 0)) as? SGGIFCollectionViewCell
        
        let expectation = self.expectation(description: "Expect completion handler to be called.")
        
        var finished = false
        let view = UIView()
        view.alpha = 1.0
        let window = UIWindow()
        window.addSubview(view)
        window.isHidden = false
        
        UIView.setAnimationsEnabled(false)
        
        cell?.startShareButtonAnimation(completion: { (isFinished) in
            finished = isFinished
            expectation.fulfill()
        })
        
        wait(for: [expectation], timeout: 2.0)
        
        XCTAssertTrue(finished)
    }
    
    func testCopyButttonAnimationFinish() {
        let nib = UINib(nibName: "SGGIFCollectionViewCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "SGGIFCollectionViewCellIdentifier")
        let cell = dataSource.collectionView(collectionView, cellForItemAt: IndexPath(row: 0, section: 0)) as? SGGIFCollectionViewCell
        
        let expectation = self.expectation(description: "Expect completion handler to be called.")
        
        var finished = false
        let view = UIView()
        view.alpha = 1.0
        let window = UIWindow()
        window.addSubview(view)
        window.isHidden = false
        
        UIView.setAnimationsEnabled(false)
        
        cell?.startCopyGifURLButtonAnimation(completion: { (isFinished) in
            finished = isFinished
            expectation.fulfill()
        })
        
        wait(for: [expectation], timeout: 2.0)
        
        XCTAssertTrue(finished)
    }
}
