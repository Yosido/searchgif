//
//  SGGifDataSourceTests.swift
//  SearchGifTDDTests
//
//  Created by Anton Sorokin on 11.03.2021.
//  Copyright © 2021 Anton Sorokin. All rights reserved.
//

import XCTest
@testable import SearchGifTDD

class SGGifDataSourceTests: XCTestCase {
    var dataSource: SGGifDataSource!
    var collectionView: UICollectionView!
    
    override func setUp() {
        let gifs: [SGGIF] = [SGGIF(gifSources: GifImages(original: Original(url: "url1", mp4: "mp4", height: "100", width: "100"), previewGif: PreviewGif(url: "previewURL"))),
                             SGGIF(gifSources: GifImages(original: Original(url: "url2", mp4: "mp4", height: "100", width: "100"), previewGif: PreviewGif(url: "previewURL"))),
                             SGGIF(gifSources: GifImages(original: Original(url: "url3", mp4: "mp4", height: "100", width: "100"), previewGif: PreviewGif(url: "previewURL")))]
        
        dataSource = SGGifDataSource(gifs: gifs)
        
        let layout = UICollectionViewFlowLayout()
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        layout.scrollDirection = .vertical
        
        collectionView.dataSource = dataSource
    }
    
    override func tearDownWithError() throws {
        
    }
    
    func testHasOneSection() {
        let numberOfSection = collectionView.numberOfSections
        XCTAssertEqual(1, numberOfSection)
        
    }
    
    func testNumberOfItemsAreTheGifsCount() {
        let numberOfItems = collectionView.numberOfItems(inSection: 0)
        XCTAssertEqual(3, numberOfItems)
    }
    
    func testItemHasCorrectGifObject() {
        let nib = UINib(nibName: "SGGIFCollectionViewCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "SGGIFCollectionViewCellIdentifier")
        
        let cell = dataSource.collectionView(collectionView, cellForItemAt: IndexPath(row: 0, section: 0)) as? SGGIFCollectionViewCell
        
        XCTAssertEqual("url1", cell?.gifObject?.gifOriginURL())
    }
    
    func testAppendGifArray() {
        dataSource.appendGifArray(data: [SGGIF(gifSources: GifImages(original: Original(url: "url3", mp4: "mp4", height: "100", width: "100"), previewGif: PreviewGif(url: "previewURL")))])
        XCTAssertEqual(dataSource.collectionView(collectionView, numberOfItemsInSection: 0), 4)
    }
    
    func testGifByIndex() {
        XCTAssertEqual(dataSource.getGifByIndex(index: 1).gifOriginURL(), "url2")
    }
    
    func testClearDataDourceGifArray() {
        dataSource.clearGifArray()
        XCTAssertEqual(dataSource.collectionView(collectionView, numberOfItemsInSection: 0), 0)
    }
}

