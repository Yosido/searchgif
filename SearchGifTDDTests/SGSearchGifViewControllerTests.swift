//
//  SGSearchGifViewControllerTests.swift
//  SearchGifTDDTests
//
//  Created by Anton Sorokin on 10.03.2021.
//  Copyright © 2021 Anton Sorokin. All rights reserved.
//

import XCTest
@testable import SearchGifTDD
import AVKit.AVPlayerViewController

class SGSearchGifViewControllerTests: XCTestCase {
    func testHasCollectionView() {
        guard let sut = createSGSearchGifViewController() else { return }
        
        sut.loadViewIfNeeded()
        
        XCTAssertNotNil(sut.resultCollectionView)
    }
    
    func test_viewDidLoadNotLoadGifsFromAPI() {
        let networkModel: SGNetworkModelSpy = SGNetworkModelSpy()
        
        guard let sut = createSGSearchGifViewController(networkModel: networkModel) else { return }
        sut.loadViewIfNeeded()

        XCTAssertEqual(networkModel.loadGifsCallCount, 0)
    }

    func test_viewWillAppear_WillLoadGifsFromAPI() {
        let networkModel: SGNetworkModelSpy = SGNetworkModelSpy()
        guard let sut = createSGSearchGifViewController(networkModel: networkModel, gifs: []) else { return }

        sut.simulateViewWillAppear()

        XCTAssertEqual(networkModel.loadGifsCallCount, 1)
    }
    
    func testSetDataSource() {
        guard let sut = createSGSearchGifViewController() else { return }

        sut.simulateViewWillAppear()

        XCTAssertEqual(sut.numberOFGifs(), 0)
    }
    
    func testLoadWithSuccessNetworkModel() {
        let gifs: [SGGIF] = GifsStub.createCorrectGifsStub()
        
        let networkModel: SGNetworkModelSpy = SGNetworkModelSpy(results: [
            .success(gifs)
        ])
        
        guard let sut = createSGSearchGifViewController(networkModel: networkModel) else { return }
        
        sut.simulateViewWillAppear()
        
        XCTAssertEqual(sut.numberOFGifs(), gifs.count)
    }
    
    func testLoadEndsWithError() {
        let networkModel: SGNetworkModelSpy = SGNetworkModelSpy(results: [
            .failure(NetworkRequestError.api(400, SGApiResultCode.kNotFoundResponseCode, "something goes wrong"))
        ])
        guard let sut = createSGSearchGifViewController(networkModel: networkModel) else { return }
        
        sut.simulateViewWillAppear()
        
        XCTAssertEqual(sut.numberOFGifs(), 0)
        XCTAssertNotNil(sut.resultCollectionView.backgroundView)
    }
    
    func testPagination() {
        let gifs: [SGGIF] = GifsStub.createCorrectGifsStub()
        
        let networkModel: SGNetworkModelSpy = SGNetworkModelSpy(results: [
            .success(gifs),
            .success(gifs)
        ])
        
        guard let sut = createSGSearchGifViewController(networkModel: networkModel) else { return }
        sut.simulateViewWillAppear()
        
        guard let cellCausePagination = sut.gifCell(at: 0) else { return }
        
        sut.collectionView(sut.resultCollectionView, willDisplay: cellCausePagination, forItemAt: IndexPath(row: 2, section: 0))
        XCTAssertEqual(sut.numberOFGifs(), 6)
    }
    

    func testPaginationWhenErrorOccured() {
        let gifs: [SGGIF] = GifsStub.createCorrectGifsStub()
        
        let networkModel: SGNetworkModelSpy = SGNetworkModelSpy(results: [
            .success(gifs),
            .failure(NetworkRequestError.api(400, SGApiResultCode.kNotFoundResponseCode, "something goes wrong"))
        ])
        
        guard let sut = createSGSearchGifViewController(networkModel: networkModel) else { return }
        sut.simulateViewWillAppear()
        
        XCTAssertEqual(sut.numberOFGifs(), gifs.count)
        
        guard let cellCausePagination = sut.gifCell(at: 0) else { return }
        sut.collectionView(sut.resultCollectionView, willDisplay: cellCausePagination, forItemAt: IndexPath(row: 2, section: 0))
        
        XCTAssertEqual(sut.numberOFGifs(), 0)
        XCTAssertNotNil(sut.resultCollectionView.backgroundView)
    }
    
    func testDidSelectItemAtIndex() {
        let gifs: [SGGIF] = GifsStub.createCorrectGifsStub()
        
        let networkModel: SGNetworkModelSpy = SGNetworkModelSpy(results: [
            .success(gifs)
        ])
        
        guard let sut = createSGSearchGifViewControllerWithoutAnimations(networkModel: networkModel, gifs: gifs) else { return }
        
        sut.simulateViewWillAppear()
        sut.selectGif(at: 0)
        
        let playerVC = sut.presentedVC as? AVPlayerViewController

        XCTAssertEqual((playerVC?.player?.currentItem?.asset as? AVURLAsset)?.url.absoluteString, "url1.mp4")
    }
    
    // MARK: - SearchBarDelegate

    func testHasSearchBar() {
        guard let sut = createSGSearchGifViewController() else { return }
        sut.simulateViewWillAppear()
        XCTAssertNotNil(sut.searchBar)
    }

    func testShouldSetSearchBarDelegate() {
        guard let sut = createSGSearchGifViewController() else { return }
        sut.simulateViewWillAppear()
        XCTAssertNotNil(sut.searchBar.delegate)
    }

    func testConformsToSearchBarDelegateProtocol() {
        guard let sut = createSGSearchGifViewController() else { return }
        sut.simulateViewWillAppear()
        XCTAssert(sut.conforms(to: UISearchBarDelegate.self))
    }
    
    func testSearchBarTextDidChangeWorks() {
        let gifs: [SGGIF] = GifsStub.createCorrectGifsStub()
        
        let networkModel: SGNetworkModelSpy = SGNetworkModelSpy(results: [
            .success(gifs),
            .success(gifs),
            .success(gifs)
        ])
        
        guard let sut = createSGSearchGifViewController(networkModel: networkModel) else { return }
        
        sut.simulateViewWillAppear()
        
        let searchBar = sut.searchBar
        var searchState = sut.searchState

        XCTAssertEqual(searchState, SGSearchState.trending)

        searchBar!.text = "newValue"
        searchBar!.delegate!.searchBar!(searchBar!, textDidChange: "newValue")
        searchState = sut.searchState
        XCTAssertEqual(searchState, SGSearchState.search)
        XCTAssertEqual(searchBar?.text, "newValue")

        searchBar!.text = ""
        searchBar!.delegate!.searchBar!(searchBar!, textDidChange: "")
        searchState = sut.searchState
        XCTAssertEqual(searchState, SGSearchState.trending)
    }
    
    func testShowErrorAfterUnsuccessfulSearch() {
        let gifs: [SGGIF] = GifsStub.createCorrectGifsStub()
        
        let networkModel: SGNetworkModelSpy = SGNetworkModelSpy(results: [
            .success(gifs),
            .failure(NetworkRequestError.api(400, SGApiResultCode.kNotFoundResponseCode, "something goes wrong"))
        ])
        
        guard let sut = createSGSearchGifViewController(networkModel: networkModel) else { return }
        
        sut.simulateViewWillAppear()
        
        let searchBar = sut.searchBar
        var searchState = sut.searchState

        //searchGifsViewCountroller.setNetworkModel(SGUnsucessNetworkModelSpy())

        searchBar!.text = "newValue"
        searchBar!.delegate!.searchBar!(searchBar!, textDidChange: "newValue")
        searchState = sut.searchState
        XCTAssertEqual(searchState, SGSearchState.search)
        XCTAssertEqual(searchBar?.text, "newValue")

        XCTAssertEqual(sut.numberOFGifs(), 0)
        XCTAssertNotNil(sut.resultCollectionView.backgroundView)
    }
}

extension SGSearchGifViewController {
    func simulateViewWillAppear() {
        loadViewIfNeeded()
        beginAppearanceTransition(true, animated: false)
    }
    
    func numberOFGifs() -> Int {
        dataSource.collectionView(resultCollectionView, numberOfItemsInSection: 0)
    }
    
    func gifCell(at row: Int) -> SGGIFCollectionViewCell? {
        let indexPath = IndexPath(row: row, section: gifsSection)
        return resultCollectionView.dataSource?.collectionView(resultCollectionView, cellForItemAt: indexPath) as? SGGIFCollectionViewCell
    }
    
    func selectGif (at row: Int) {
        let indexPath = IndexPath(row: row, section: gifsSection)
        resultCollectionView.delegate?.collectionView?(resultCollectionView, didSelectItemAt: indexPath)
    }
    
    private var gifsSection: Int { 0 }
}
