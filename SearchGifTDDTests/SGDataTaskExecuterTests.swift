//
//  SGDataTaskExecuterTests.swift
//  SearchGifTDDTests
//
//  Created by Anton Sorokin on 23.03.2021.
//  Copyright © 2021 Anton Sorokin. All rights reserved.
//

import XCTest

import XCTest
@testable import SearchGifTDD
//
class SGDataTaskExecuterTests: XCTestCase {
    override func tearDown() {
        TestURLProtocol.loadingHandler = nil
    }
    
    func testRequestWithoutApiKeyFailure() {
        let request = SGURLRequestConstructor.requestWithoutAPIKey()
        
        TestURLProtocol.loadingHandler = { request in
            let response = HTTPURLResponse(url: request.url!, statusCode: 200, httpVersion: nil, headerFields: nil)!
            return (response, Data("{\"message\":\"Invalid authentication credentials\"}\n".utf8), //nil)
                    NetworkRequestError.api(404, SGApiResultCode.kBadRequestResponseCode, "lol kek"))
        }
        
        let expectation = XCTestExpectation(description: "Loading")
        let configuration = URLSessionConfiguration.ephemeral
        configuration.protocolClasses = [TestURLProtocol.self]
        
        let dataTaskExecutor = SGDataTaskExecuter(request: request, session:URLSession(configuration: configuration))
        dataTaskExecutor.fetchGifsRequest { (result) in
            switch result {
            case .failure(_):
                XCTFail("Request was fail when it was expected not to.")
            case .success(let data):
                XCTAssertEqual("{\"message\":\"Invalid authentication credentials\"}\n",
                               String(decoding: data, as: UTF8.self))
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1)
    }
    
    func testSuccessfulRequest() {
        let request = SGURLRequestConstructor.request()
        TestURLProtocol.loadingHandler = { request in
            let response = HTTPURLResponse(url: request.url!, statusCode: 200, httpVersion: nil, headerFields: nil)!
            return (response, fixtureCorrectJSON, nil)
        }
        
        let expectation = XCTestExpectation(description: "Loading")
        let configuration = URLSessionConfiguration.ephemeral
        configuration.protocolClasses = [TestURLProtocol.self]
        
        let dataTaskExecutor = SGDataTaskExecuter(request: request, session:URLSession(configuration: configuration))
        
        dataTaskExecutor.fetchGifsRequest { (result) in
            switch result {
            case .failure(let _):
                XCTFail("Request shouldn't not fail when it was expected to.")
            case .success(let responseData):
                XCTAssertEqual(responseData, fixtureCorrectJSON)
            }
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 10)
    }
    
    func testNetworkErrorFailure() {
        let request = SGURLRequestConstructor.requestWithoutAPIKey()
        TestURLProtocol.loadingHandler = { request in
            let response = HTTPURLResponse(url: request.url!, statusCode: 404, httpVersion: nil, headerFields: nil)!
            return (response, nil, NetworkRequestError.api(404, SGApiResultCode.kNotFoundResponseCode, "something goes wrong"))
        }
        
        let expectation = XCTestExpectation(description: "Loading")
        let configuration = URLSessionConfiguration.ephemeral
        configuration.protocolClasses = [TestURLProtocol.self]
        
        let dataTaskExecutor = SGDataTaskExecuter(request: request, session:URLSession(configuration: configuration))
        
        dataTaskExecutor.fetchGifsRequest { (result) in
            switch result {
            case .failure(let error):
                XCTAssertTrue(error is Error)
            case .success(_):
                XCTFail("Request did not fail when it was expected to.")
            }
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 10)
    }
}

extension SGURLRequestConstructor {
    class func requestWithoutAPIKey() -> URLRequest {
        
        var components = URLComponents()
        components.scheme = "https"
        components.host = "api.giphy.com"
        components.path = "/v1/gifs/search"
        components.queryItems = [
            URLQueryItem(name: "api_key", value: ""),
            URLQueryItem(name: "q", value: "searchTerm"),
            URLQueryItem(name: "limit", value: "10"),
            URLQueryItem(name: "offset", value: String("10"))
        ]
        
        return URLRequest(url: components.url!)
    }
    
    class func request() -> URLRequest {
        
        var components = URLComponents()
        components.scheme = "https"
        components.host = "api.giphy.com"
        components.path = "/v1/gifs/search"
        components.queryItems = [
            URLQueryItem(name: "api_key", value: "qqq"),
            URLQueryItem(name: "q", value: "searchTerm"),
            URLQueryItem(name: "limit", value: "10"),
            URLQueryItem(name: "offset", value: String("10"))
        ]
        
        return URLRequest(url: components.url!)
    }
}


final class TestURLProtocol: URLProtocol {
    override class func canInit(with request: URLRequest) -> Bool {
        return true
    }
    
    override class func canonicalRequest(for request: URLRequest) -> URLRequest {
        return request
    }
    
    static var loadingHandler: ((URLRequest) -> (HTTPURLResponse, Data?, Error?))?
    
    override func startLoading() {
        guard let handler = TestURLProtocol.loadingHandler else {
            XCTFail("Loading handler is not set.")
            return
        }
        let (response, data, error) = handler(request)
        if let data = data {
            client?.urlProtocol(self, didReceive: response, cacheStoragePolicy: .notAllowed)
            client?.urlProtocol(self, didLoad: data)
            client?.urlProtocolDidFinishLoading(self)
        }
        else {
            client?.urlProtocol(self, didFailWithError: error!)
        }
    }
    
    override func stopLoading() {
    }
}

private let fixtureCorrectJSON = Data("""
{
   "data":[
      {
         "type":"gif",
         "id":"3o6Ztp5m2kEyxCo5sA",
         "url":"https://giphy.com/gifs/southparkgifs-3o6Ztp5m2kEyxCo5sA",
         "slug":"southparkgifs-3o6Ztp5m2kEyxCo5sA",
         "bitly_gif_url":"http://gph.is/2bSKTqA",
         "bitly_url":"http://gph.is/2bSKTqA",
         "embed_url":"https://giphy.com/embed/3o6Ztp5m2kEyxCo5sA",
         "username":"southpark",
         "source":"http://comedycentral.com",
         "title":"scared eric cartman GIF by South Park ",
         "rating":"pg-13",
         "content_url":"",
         "source_tld":"comedycentral.com",
         "source_post_url":"http://comedycentral.com",
         "is_sticker":0,
         "import_datetime":"2016-08-23 18:25:33",
         "trending_datetime":"1970-01-01 00:00:00",
         "images":{
            "original":{
               "height":"270",
               "width":"480",
               "size":"865941",
               "url":"https://media4.giphy.com/media/3o6Ztp5m2kEyxCo5sA/giphy.gif?cid=e1bb72ffauam6l1yi6k678ns64m31xheyim52a3r5eyrcn0f&rid=giphy.gif",
               "mp4_size":"261914",
               "mp4":"https://media4.giphy.com/media/3o6Ztp5m2kEyxCo5sA/giphy.mp4?cid=e1bb72ffauam6l1yi6k678ns64m31xheyim52a3r5eyrcn0f&rid=giphy.mp4",
               "webp_size":"918666",
               "webp":"https://media4.giphy.com/media/3o6Ztp5m2kEyxCo5sA/giphy.webp?cid=e1bb72ffauam6l1yi6k678ns64m31xheyim52a3r5eyrcn0f&rid=giphy.webp",
               "frames":"142",
               "hash":"0a7fc2bde5138b090355525099b7a50b"
            },
            "preview_gif":{
               "height":"98",
               "width":"174",
               "size":"46521",
               "url":"https://media4.giphy.com/media/3o6Ztp5m2kEyxCo5sA/giphy-preview.gif?cid=e1bb72ffauam6l1yi6k678ns64m31xheyim52a3r5eyrcn0f&rid=giphy-preview.gif"
            }
         }
      }
   ],
   "pagination":{
      "total_count":1,
      "count":1,
      "offset":0
   },
   "meta":{
      "status":200,
      "msg":"OK",
      "response_id":"auam6l1yi6k678ns64m31xheyim52a3r5eyrcn0f"
   }
}
""".utf8)
