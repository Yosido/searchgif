//
//  UICollectionViewAdditions.swift
//  SearchGifTDDTests
//
//  Created by Anton Sorokin on 23.03.2021.
//  Copyright © 2021 Anton Sorokin. All rights reserved.
//

import XCTest
@testable import SearchGifTDD

class UICollectionViewAdditions: XCTestCase {
    var dataSource: SGGifDataSource!
    var collectionView: UICollectionView!
    
    override func setUp() {
        let gifs: [SGGIF] = [SGGIF(gifSources: GifImages(original: Original(url: "url1", mp4: "mp4", height: "100", width: "100"), previewGif: PreviewGif(url: "previewURL"))),
                             SGGIF(gifSources: GifImages(original: Original(url: "url2", mp4: "mp4", height: "100", width: "100"), previewGif: PreviewGif(url: "previewURL"))),
                             SGGIF(gifSources: GifImages(original: Original(url: "url3", mp4: "mp4", height: "100", width: "100"), previewGif: PreviewGif(url: "previewURL")))]
        
        dataSource = SGGifDataSource(gifs: gifs)
        
        let layout = UICollectionViewFlowLayout()
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        layout.scrollDirection = .vertical
        
        collectionView.dataSource = dataSource
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testShowBackgroundMessage() {
        let error = NetworkRequestError.api(404, SGApiResultCode.kNotFoundResponseCode, "something goes wrong")
        collectionView.updateCollectionViewWithError(error: error)
        //RunLoop.main.run(until: Date(timeIntervalSinceNow: 0.1))
        XCTAssertNotNil(collectionView.backgroundView)
        //RunLoop.main.run(until: Date(timeIntervalSinceNow: 0.1))
        collectionView.hideEmptyMessageIfNeeded()
        XCTAssertNil(collectionView.backgroundView)
    }
    
    func testScrollToTop() {
        collectionView.setContentOffset(CGPoint(x: 0, y: 100), animated: false)
        XCTAssertEqual(collectionView.contentOffset.y, 100)
        collectionView.scrollCollectionViewToTop()
        XCTAssertEqual(collectionView.contentOffset.y, 0)
    }
}
