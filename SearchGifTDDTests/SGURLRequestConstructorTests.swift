//
//  SGURLRequestConstructorTests.swift
//  SearchGifTDDTests
//
//  Created by Anton Sorokin on 24.03.2021.
//  Copyright © 2021 Anton Sorokin. All rights reserved.
//

import XCTest
@testable import SearchGifTDD

class SGURLRequestConstructorTests: XCTestCase {
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testSearchRequest() {
        let constructor = SGURLRequestConstructor(searchTerm: "", searchState: SGSearchState.search, offset: 0)
        let request = constructor.request()
        
        let requstString = request.url?.absoluteString
        
        XCTAssertTrue(requstString!.contains("https://api.giphy.com/v1/gifs/search?api_key="))
    }
    
    func testTrendingRequest() {
        let constructor = SGURLRequestConstructor(searchTerm: "", searchState: SGSearchState.trending, offset: 0)
        let request = constructor.request()
        
        let requstString = request.url?.absoluteString
        XCTAssertTrue(requstString!.contains("https://api.giphy.com/v1/gifs/trending?api_key="))
    }
}

