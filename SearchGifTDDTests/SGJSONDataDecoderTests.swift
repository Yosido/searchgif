//
//  SGJSONDataDecoderTests.swift
//  SearchGifTDDTests
//
//  Created by Anton Sorokin on 23.03.2021.
//  Copyright © 2021 Anton Sorokin. All rights reserved.
//

import XCTest
@testable import SearchGifTDD

class SGJSONDataDecoderTests: XCTestCase {
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testDecodeCorrectEmptyJSON() {
        let decoder = SGJSONDataDecoder(data: fixtureCorrectEmptyJSON)
        decoder.decodeJSONDataToResponseObject { (result) in
            switch result {
            case .success(let gifArray):
                XCTAssertNotNil(gifArray)
                XCTAssertTrue(gifArray.count == 0)
            case .failure(_):
                XCTFail("decode shouldnt ended as failure")
            }
        }
    }
    
    func testDecodeIncorrectJSON() {
        let decoder = SGJSONDataDecoder(data: fixtureIncorrectEmptyJSON)
        decoder.decodeJSONDataToResponseObject { (result) in
            switch result {
            case .success(_):
                XCTFail("decode shouldnt ended as successfully")
            case .failure(let error):
                XCTAssertNotNil(error)
            }
        }
    }
    
    func testDecodeCorrectJSONWithServerError() {
        let decoder = SGJSONDataDecoder(data: fixtureCorrectEmptyJSONWithServerError)
        decoder.decodeJSONDataToResponseObject { (result) in
            
            switch result {
            case .failure(NetworkRequestError.api(_, let resultCode, _)):
                XCTAssert(resultCode == .kBadRequestResponseCode)
            case .success(_):
                XCTFail()
            case .failure(_):
                XCTFail()
            }
        }
    }
    
    func testDecodeCorrectJSONWithUnknownError() {
        let decoder = SGJSONDataDecoder(data: fixtureCorrectEmptyJSONWithUnknownServerError)
        decoder.decodeJSONDataToResponseObject { (result) in
            switch result {
            case .success:
                XCTFail("decode shouldnt ended as successfully")
            case .failure(let error):
                XCTAssertNotNil(error)
            }
        }
    }
}

private let fixtureCorrectEmptyJSON = Data("""
{"data":[],"pagination":{"total_count":0,"count":0,"offset":0},"meta":{"status":200,"msg":"OK","response_id":"mxpdc9mwun58ca4sto5dl0l0aix8dmnab6yisztn"}}
""".utf8)

private let fixtureIncorrectEmptyJSON = Data("""
{"data":[],"pagination"::{"total_count":0,"count":0,"offset":0},"meta":{"status":200,"msg":"OK","response_id":"mxpdc9mwun58ca4sto5dl0l0aix8dmnab6yisztn"}}
""".utf8)

private let fixtureCorrectEmptyJSONWithServerError = Data("""
{"data":[],"pagination":{"total_count":0,"count":0,"offset":0},"meta":{"status":400,"msg":"Bad Request","response_id":"mxpdc9mwun58ca4sto5dl0l0aix8dmnab6yisztn"}}
""".utf8)

private let fixtureCorrectEmptyJSONWithUnknownServerError = Data("""
{"data":[],"pagination":{"total_count":0,"count":0,"offset":0},"meta":{"status":999,"msg":"Error From Future","response_id":"mxpdc9mwun58ca4sto5dl0l0aix8dmnab6yisztn"}}
""".utf8)
